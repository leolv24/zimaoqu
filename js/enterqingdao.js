window.onload = function () {
    (function (doc, win) {
        var docEl = doc.documentElement,
            resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
            recalc = function () {
                var clientWidth = docEl.clientWidth;
                if (!clientWidth) return;
                if (clientWidth > 1199) {
                    docEl.style.fontSize = 20 * (clientWidth / 256) + 'px';
                }
            };
        if (!doc.addEventListener) return;
        win.addEventListener(resizeEvt, recalc, false);
        doc.addEventListener('DOMContentLoaded', recalc, false)
        recalc();
    })(document, window);

    
    let beforeSelect = 0;
    const menuEnterQing = document.getElementsByClassName("enterqingdao");
    const tabs = document.getElementsByClassName("tab");
    const sections = document.getElementsByTagName("section");

    // 点击页签切换
    document.getElementById("tabs").addEventListener("click", function (eve) {
        const index = eve.target.getAttribute("index") || eve.target.parentNode.getAttribute("index");
        changeTab(index);
    })

    // 点击菜单切换
    document.getElementById("enterqingdao").addEventListener("click", function (eve) {
        const index = eve.target.getAttribute("index") || eve.target.parentNode.getAttribute("index");
        window.location.href = window.location.href.split("#")[0] + "#tabs"
        changeTab(index);
    })

    /**
     * 点击菜单或页签，切换页面
     * @param {number} index 
     * 第几个tab页，（0,3）
     */
    function changeTab(index) {
        menuEnterQing[beforeSelect].className = "enterqingdao"
        tabs[beforeSelect].className = "tab";
        sections[beforeSelect].className = "";
        menuEnterQing[index].className = "enterqingdao active"
        tabs[index].className = "tab active";
        sections[index].className = "active";
        beforeSelect = index;
    }

    // 其他页面点击菜单跳转到本页面，解析url，获取index，切换到对应tab页, 没有获取到，显示第0个
    let firstTab = Number(window.location.href.split("index=")[1])
    let first = firstTab ? firstTab - 1 : 0;
    changeTab(first);

    // video
    var options = {
        src:"../assets/video/vedio1.mp4",
        autoplay: true,
        controls: false,
        loop:true
    };

    var player = videojs('my-player', options, function onPlayerReady() {
        videojs.log('Your player is ready!');

        this.on('ended', function () {
            videojs.log('Awww...over so soon?!');
        });
    });

    
    // 投资环境，优势菜单切换效果
    let envSelect = 0;
    const envMenu = [...document.getElementsByClassName("environment-menu")];
    const envDetail = [...document.getElementsByClassName("environment-detail")];

    envMenu.forEach((item,i) => {
        item.addEventListener("click", function(e){
            const index = e.currentTarget.getAttribute("index") || e.currentTarget.parentNode.getAttribute("index")
            envMenu[envSelect].className = "environment-menu";
            envMenu[index].className = "environment-menu active";
            envDetail[envSelect].className = "environment-detail";
            envDetail[index].className = "environment-detail active";
            envSelect = index;
        })
    })
    
    // 重点产业菜单切换
    let devSelect = 0;
    const devMenu = [...document.getElementsByClassName("industry-menu")];
    const devDetail = [...document.getElementsByClassName("industry-detail")];

    devMenu.forEach((item,i) => {
        item.addEventListener("click", function(e){
            const index = e.currentTarget.getAttribute("index") || e.currentTarget.parentNode.getAttribute("index")
            console.log("index:", index)
            devMenu[devSelect].className = "industry-menu";
            devMenu[index].className = "industry-menu active";
            devDetail[devSelect].className = "industry-detail";
            devDetail[index].className = "industry-detail active";
            devSelect = index;
        })
    })
    
    if(tabIndex != undefined && tabIndex.length>0){
        changeTab(tabIndex-1);
    }
};