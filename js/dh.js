// 创建一个可以执行简单动画的函数
// 参数1：要执行动画的对象（obj）;
// 参数2：要执行动画的样式,可以是left,right,top等（attr）；
// 参数3：执行动画的目标位置（target）；
// 参数4：表示移动的速度正数向右移动，负数向左移动（speed）；
// 参数5：回调函数（callback），这个函数将会在动画执行完毕以后执行
function picMove(obj, attr, target, speed, callback) {

    //在开启一个定时器之前，先关闭上一个元素的定时器
    clearInterval(obj.timer);
    //获取元素目前的位置
    var current = parseInt(getStyle(obj, attr));
    //判断速度的正负
    //如果从0向800移动，则speed为正，如果从800向0移动，则speed为负
    if (current > target) {
        speed = -speed;
    }
    //开启一个定时器来实现动画效果
    //向执行动画的对象中添加一个timer属性值，用来保存它自己的定时器标识
    obj.timer = setInterval(function () {

        //获取div的原来的值
        var oldvalue = parseInt(getStyle(obj, attr));
        //在旧值的基础上增加
        var newvalue = oldvalue + speed;
        //判读newvalue是否大于800
        if (speed < 0 && newvalue < target || speed > 0 && newvalue > target) {
            newvalue = target;
        }
        //将新值赋给box
        obj.style[attr] = newvalue + "px";
        //当元素移动到800px时，则停止执行动画
        if (newvalue == target) {
            //达到目标关闭定时器
            clearInterval(obj.timer);
            //动画执行完毕，调用回调函数
            callback && callback();
        }
    }, 30);
}

//定义一个函数用来获取指定元素的当前样式，参数，obj代表需获取样式的元素，name代表需获取的样式名
function getStyle(obj, name) {
    if (window.getComputedStyle) {
        //正常浏览器有getComputedStyle()方法
        return getComputedStyle(obj, null)[name];
    }
    else {
        //IE8的浏览器没有getComputedStyle()方法
        return obj.currentStyle[name];
    }
}

// 轮播
/**
 * 
 * @param {*} obj 
 * pContainer ul外层div的ID
 * pId      图片li的外层ul的ID 
 * picClass   图片li的class
 * picWidth     图片宽度
 * navBlock     下方原点标签外层block的ID
 * navClass     下方圆点的class
 * navSelect    圆点选中后颜色
 * time         多久自动切换一张图片
 */
function swipper(obj,timer){
    //获取ul
    var imgList = document.getElementById(obj.pId);
    //获取页面中所有的图片(即获取img标签)
    var imgarr = document.getElementsByClassName(obj.picClass);
    //设置imgList的宽度
    imgList.style.width = obj.picWidth * imgarr.length + "px";
    //设置导航按钮居中
    //第一步获取导航div
    var navdiv = document.getElementById(obj.navBlock);
    var outer = document.getElementById(obj.pContainer);
    //第二步设置left值，left=（outer的宽度-navdiv的宽度）/2
    navdiv.style.left = (outer.offsetWidth - navdiv.offsetWidth) / 2 + "px";
    //默认显示图片的的索引
    var index = 0;
    //获取所有的a
    var alla = document.getElementsByClassName(obj.navClass);
    //设置默认选中的效果
    alla[index].style.backgroundColor = obj.navSelect || "#ffed57";
    //点击超链接切换到指定图片，点击第一个超链接显示第一张图片，点击第二个超链接显示第二张图片
    //为所有超链接都绑定单击响应事件
    for (var i = 0; i < alla.length; i++) {
      //为每一个超链接添加一个num属性
      alla[i].num = i;
      alla[i].onclick = function () {
        //关闭定时器
        clearInterval(timer);
        //获取点击超链接的索引,并将其设置为index
        index = this.num;
        //切换图片,第一张索引为0，left为0，第二张索引为1，left为-1366，第三张索引为2，left为-1366*2
        //imgList.style.left=-1366*index+"px";
        //设置选中a
        setA();
        //使用move函数来切换图片
        picMove(imgList, "left", -1 * obj.picWidth * index, 200, function () {
          //动画结束，开启自动切换
          autoChange();
        });
      };
    }
    
    //创建一个方法用来设置选中的a
    function setA() {
      //判断当前索引是否是最后一张图片
      if (index >= imgarr.length - 1) {//则将index设置为0
        index = 0;
        //此时显示的是最后一张图片，而最后一张图片和第一张是一样的通过CSS将最后一张切换成第一张
        imgList.style.left = 0;

      }
      //遍历所有的a并将所有的背景颜色都设置为红色
      for (var i = 0; i < alla.length; i++) {
        alla[i].style.backgroundColor = "";
      }
      //将选中的a设置为黑色
      alla[index].style.backgroundColor = "#ffed57";
    };
    if (timer) {
      console.log("重新设置timer")
      clearInterval(timer);
      timer = null;
      index = 0;
      // 图片显示到第一张
      imgList.style.left = 0;
      alla[index].style.backgroundColor = "#ffed57";
    }
    //自动切换图片
    autoChange();
    //创建一个函数，用来开启自动切换图片
    function autoChange() {
      //开启一个定时器用来定时切换图片
      timer = setInterval(function () {
        //使索引自增
        index++;
        index = index % imgarr.length;
        //执行动画,切换图片
        picMove(imgList, "left", obj.picWidth * -1 * index, 20, function () {
          //修改导航按钮
          setA();
        });
      }, obj.time || 3000);
    }
}
