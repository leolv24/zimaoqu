window.onload = function () {
    (function (doc, win) {
        var docEl = doc.documentElement,
            resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
            recalc = function () {
                var clientWidth = docEl.clientWidth;
                if (!clientWidth) return;
                if (clientWidth > 767) {
                    docEl.style.fontSize = 20 * (clientWidth / 256) + 'px';
                }
            };
        if (!doc.addEventListener) return;
        win.addEventListener(resizeEvt, recalc, false);
        doc.addEventListener('DOMContentLoaded', recalc, false)
        recalc();
    })(document, window);
    
  let hot = document.getElementsByClassName("service-type-hot")[0];
  let common = document.getElementsByClassName("service-type-common")[0];
  let person = document.getElementsByClassName("service-type-person")[0];
  let hot_d = document.getElementsByClassName("hot-d")[0];
  let news_d = document.getElementsByClassName("news-d")[0];
  let person_d = document.getElementsByClassName("person-d")[0];

  hot.addEventListener("mouseover", function(eve) {
    hot.className = "service-type-hot active";
    common.className = "service-type-common";
    person.className = "service-type-person";
    hot_d.className = "hot-d";
    news_d.className = "news-d _hidden";
    person_d.className = "person-d _hidden"
  });
  common.addEventListener("mouseover", function(eve){
    hot.className = "service-type-hot";
    common.className = "service-type-common active";
    person.className = "service-type-person";
    hot_d.className = "hot-d _hidden";
    news_d.className = "news-d";
    person_d.className = "person-d _hidden"
  });
  person.addEventListener("mouseover", function(eve){
    hot.className = "service-type-hot";
    common.className = "service-type-common";
    person.className = "service-type-person active";
    hot_d.className = "hot-d _hidden";
    news_d.className = "news-d _hidden";
    person_d.className = "person-d"
  });
};