window.onload = function () {
    (function (doc, win) {
        var docEl = doc.documentElement,
            resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
            recalc = function () {
                var clientWidth = docEl.clientWidth;
                if (!clientWidth) return;
                if (clientWidth > 767) {
                    docEl.style.fontSize = 20 * (clientWidth / 256) + 'px';
                }
            };
        if (!doc.addEventListener) return;
        win.addEventListener(resizeEvt, recalc, false);
        doc.addEventListener('DOMContentLoaded', recalc, false)
        recalc();
    })(document, window);

    layui.use('laypage', function () {
        var laypage = layui.laypage
            , layer = layui.layer;

        //执行一个laypage实例
        laypage.render({
            elem: 'pages'
            , count: 100
            ,groups: 4
            ,theme: '#EB4B3F'
            , layout: ['prev', 'page', 'next', 'skip']
            , jump: function (obj) {
                console.log(obj)
            }
        });
    });
};