window.onload = function () {
  (function (doc, win) {
    var docEl = doc.documentElement,
      resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
      recalc = function () {
        var clientWidth = docEl.clientWidth;
        if (!clientWidth) return;
        if (clientWidth > 1199) {
          docEl.style.fontSize = 20 * (clientWidth / 384) + 'px';
        }
      };
    if (!doc.addEventListener) return;
    win.addEventListener(resizeEvt, recalc, false);
    doc.addEventListener('DOMContentLoaded', recalc, false)
    recalc();
  })(document, window);

  /*
  let hot = document.getElementsByClassName("service-type-hot")[0];
  let common = document.getElementsByClassName("service-type-common")[0];
  let person = document.getElementsByClassName("service-type-person")[0];
  let hot_d = document.getElementsByClassName("hot-d")[0];
  let news_d = document.getElementsByClassName("news-d")[0];
  let person_d = document.getElementsByClassName("person-d")[0];

  hot.addEventListener("mouseover", function(eve) {
    hot.className = "service-type-hot active";
    common.className = "service-type-common";
    person.className = "service-type-person";
    hot_d.className = "hot-d";
    news_d.className = "news-d _hidden";
    person_d.className = "person-d _hidden"
  });
  common.addEventListener("mouseover", function(eve){
    hot.className = "service-type-hot";
    common.className = "service-type-common active";
    person.className = "service-type-person";
    hot_d.className = "hot-d _hidden";
    news_d.className = "news-d";
    person_d.className = "person-d _hidden"
  });
  person.addEventListener("mouseover", function(eve){
    hot.className = "service-type-hot";
    common.className = "service-type-common";
    person.className = "service-type-person active";
    hot_d.className = "hot-d _hidden";
    news_d.className = "news-d _hidden";
    person_d.className = "person-d"
  });*/

  let bsService = document.getElementsByClassName("business-service")[0];
  let psService = document.getElementsByClassName("person-service")[0];
  let bs_d = document.getElementsByClassName("business-service-details")[0];
  let psd_d = document.getElementsByClassName("talent-service-details")[0];

  bsService.addEventListener("mouseover", function(eve) {
    bsService.className = "business-service choose-style";
    psService.className = "person-service";
    bs_d.style.display = "block";
    psd_d.style.display = "none";
  });
  psService.addEventListener("mouseover", function(eve){
    bsService.className = "business-service";
    psService.className = "person-service choose-style";
    bs_d.style.display = "none";
    psd_d.style.display = "block";
  });

  
  let left = document.getElementById("arrow-left");
  let right = document.getElementById("arrow-right");
  left.addEventListener("click", function(eve){
    let items = [...document.getElementsByClassName("features-item")];
    let tmpNode = items[0];
    tmpNode.style.display = "none"
    items[4].style.display = "list-item"
    items[0].parentNode.removeChild(items[0]);
    items[1].parentNode.appendChild(tmpNode);
  })

  right.addEventListener("click", function(eve){
    let items = [...document.getElementsByClassName("features-item")];
    let tmpNode = items[10];
    items[3].style.display = "none";
    items[10].style.display = "list-item";
    items[0].parentNode.removeChild(items[10]);
    items[0].parentNode.prepend(tmpNode);
    
    
  })
};